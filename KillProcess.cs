﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace ProcessKiller
{
    class killProcess
    {
        public static void stopNamedProcess(string name)
        {
            foreach (Process p in System.Diagnostics.Process.GetProcessesByName(name))
            {
                try
                {
                    p.Kill();
                    p.WaitForExit();
                }
                catch (Exception exp)
                {
                    Console.WriteLine(exp.Message);
                    System.Diagnostics.EventLog.WriteEntry("ProcessKiller ", exp.Message, System.Diagnostics.EventLogEntryType.Error);
                }

            }

        }


	public static void bounceServer(string serverName)
	{
		// No changes to direct method...


	}
    }

}
